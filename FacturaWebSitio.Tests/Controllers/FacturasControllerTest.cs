﻿using FacturaWebServicio.Models;
using FacturaWebSitio.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FacturaWebSitio.Tests.Controllers
{
	[TestClass]
	public class FacturasControllerTest
	{
		[TestMethod]
		public void Index()
		{
			throw new NotImplementedException();
		}

		[TestMethod]
		public void Edit(int id)
		{
			if (id < 1)
				return RedirectToAction("Index");
			Factura factura = new Factura();
			factura.IdFactura = id;
			return View("Edit", factura);
		}
	}
}
