﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FacturaWebServicio.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class FacturaWebEntities : DbContext
    {
        public FacturaWebEntities()
            : base("name=FacturaWebEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Factura> Factura { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }
    
        public virtual int CrearFactura_sp(string numeroContrato, string descripcion, Nullable<int> idUsuario)
        {
            var numeroContratoParameter = numeroContrato != null ?
                new ObjectParameter("NumeroContrato", numeroContrato) :
                new ObjectParameter("NumeroContrato", typeof(string));
    
            var descripcionParameter = descripcion != null ?
                new ObjectParameter("Descripcion", descripcion) :
                new ObjectParameter("Descripcion", typeof(string));
    
            var idUsuarioParameter = idUsuario.HasValue ?
                new ObjectParameter("IdUsuario", idUsuario) :
                new ObjectParameter("IdUsuario", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("CrearFactura_sp", numeroContratoParameter, descripcionParameter, idUsuarioParameter);
        }
    
        public virtual ObjectResult<Factura> ConsultarFacturas_sp()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Factura>("ConsultarFacturas_sp");
        }
    
        public virtual ObjectResult<Factura> ConsultarFacturas_sp(MergeOption mergeOption)
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Factura>("ConsultarFacturas_sp", mergeOption);
        }
    
       
        public virtual int EditarFactura_sp(Nullable<int> idFactura, string descripcion)
        {
            var idFacturaParameter = idFactura.HasValue ?
                new ObjectParameter("IdFactura", idFactura) :
                new ObjectParameter("IdFactura", typeof(int));
    
            var descripcionParameter = descripcion != null ?
                new ObjectParameter("Descripcion", descripcion) :
                new ObjectParameter("Descripcion", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("EditarFactura_sp", idFacturaParameter, descripcionParameter);
        }
    
        public virtual int EliminarFactura_sp(Nullable<int> idFactura)
        {
            var idFacturaParameter = idFactura.HasValue ?
                new ObjectParameter("IdFactura", idFactura) :
                new ObjectParameter("IdFactura", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("EliminarFactura_sp", idFacturaParameter);
        }
    
        public virtual ObjectResult<Nullable<int>> ConsultarFacturaNumContrato_sp(string numContrato)
        {
            var numContratoParameter = numContrato != null ?
                new ObjectParameter("NumContrato", numContrato) :
                new ObjectParameter("NumContrato", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("ConsultarFacturaNumContrato_sp", numContratoParameter);
        }
    }
}
