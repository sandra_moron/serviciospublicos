﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FacturaWebServicio.Models
{
	public class FacturaDbContext : DbContext
	{
		public DbSet<Factura> Facturas { get; set; }

		//protected override void OnModelCreating(DbModelBuilder modelBuilder)
		//{
		//	base.OnModelCreating(modelBuilder);
		//}
	}
}