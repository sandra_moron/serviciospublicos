﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using FacturaWebServicio.Models;
using System.Data.SqlClient;

namespace FacturaWebServicio.Controllers
{
    public class FacturasController : ApiController
    {
        private FacturaWebEntities db = new FacturaWebEntities();

        // GET: api/Facturas
        public IEnumerable<Factura> GetFactura()
        {			
            return  db.Database.SqlQuery<Factura>("ConsultarFacturas_sp").ToList();
		}

        // GET: api/Facturas/5
        [ResponseType(typeof(Factura))]
        public IHttpActionResult GetFactura(int id)
        {
		    Factura factura = db.Database.SqlQuery<Factura>("ObtenerFactura_sp @IdFactura", new SqlParameter("@IdFactura", id)).ToList().FirstOrDefault();
			
			if (factura == null)
            {
                return NotFound();
            }

            return Ok(factura);
        }

        // PUT: api/Facturas/5
        [ResponseType(typeof(void))]
	
		public IHttpActionResult PutFactura(Factura factura)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }			        
			          
            try
            {
				db.EditarFactura_sp(factura.IdFactura, factura.Descripcion);			
			}
            catch (Exception)
            {                
                    return BadRequest();               
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Facturas
        [ResponseType(typeof(Factura))]
        public IHttpActionResult PostFactura(Factura factura)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

			try
			{
				db.CrearFactura_sp(factura.NumeroContrato, factura.Descripcion, factura.IdUsuario);
								
			}
			catch (Exception ex)
			{
				return BadRequest();
			}
			

            return CreatedAtRoute("DefaultApi", new { id = factura.IdFactura }, factura);
        }

        // DELETE: api/Facturas/5
        [ResponseType(typeof(Factura))]
        public IHttpActionResult DeleteFactura(int id)
        {
            Factura factura = db.Factura.Find(id);
            if (factura == null)
            {
                return NotFound();
            }

			try
			{
				db.EliminarFactura_sp(id);
			
			}
			catch (Exception)
			{

				throw;
			}
          

            return Ok(factura);
        }

		[HttpGet]
		[Route("api/Facturas/ExisteFactura/{numContrato}")]
		public IHttpActionResult ExisteFactura(string numContrato)
		{	
			try
			{
				var existe = db.ConsultarFacturaNumContrato_sp(numContrato).FirstOrDefault();
				return Ok(existe);
			}
			catch (Exception)
			{
				return BadRequest();
			}	

		}

		protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FacturaExists(int id)
        {
            return db.Factura.Count(e => e.IdFactura == id) > 0;
        }
    }
}