
-- =============================================
-- Author:		Sandra Moron
-- Create date: 27/06/2017
-- Description:	Consultar una Factura
-- =============================================


IF OBJECT_ID('dbo.ObtenerFactura_sp') IS NOT NULL
BEGIN
   DROP PROCEDURE ObtenerFactura_sp;
END 
go
create PROCEDURE [dbo].[ObtenerFactura_sp]
 @IdFactura int 
AS
BEGIN
	SELECT [IdFactura]
      ,[NumeroContrato]
      ,[Descripcion]
      ,[IdUsuario]
  FROM [dbo].[Factura]
   WHERE IdFactura =@IdFactura
END
