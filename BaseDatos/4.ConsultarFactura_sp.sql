
-- =============================================
-- Author:		Sandra Moron
-- Create date: 27/06/2017
-- Description:	Consultar Factura
-- =============================================


IF OBJECT_ID('dbo.ConsultarFactura_sp') IS NOT NULL
BEGIN
   DROP PROCEDURE CrearFactura_sp;
END 

go
CREATE PROCEDURE ConsultarFactura_sp
 
AS
BEGIN
	SELECT [IdFactura]
      ,[NumeroContrato]
      ,[Descripcion]
      ,[IdUsuario]
  FROM [dbo].[Factura]

END
GO
