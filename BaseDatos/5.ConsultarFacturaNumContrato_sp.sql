
-- =============================================
-- Author:		Sandra Moron
-- Create date: 28/06/2017
-- Description:	Consultar si existe Factura con un numero de Contrato
-- =============================================


IF OBJECT_ID('dbo.ConsultarFacturaNumContrato_sp') IS NOT NULL
BEGIN
   DROP PROCEDURE ConsultarFacturaNumContrato_sp;
END 

go
CREATE PROCEDURE ConsultarFacturaNumContrato_sp
	@NumContrato nvarchar(50)
AS
BEGIN
	SELECT [IdFactura]
      ,[NumeroContrato]
      ,[Descripcion]
      ,[IdUsuario]
  FROM [dbo].[Factura]
  where NumeroContrato = @NumContrato

END
GO
