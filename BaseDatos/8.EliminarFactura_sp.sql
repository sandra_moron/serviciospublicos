
-- =============================================
-- Author:		Sandra Moron
-- Create date: 27/06/2017
-- Description:	eliminar una Factura
-- =============================================


IF OBJECT_ID('dbo.EliminarFactura_sp') IS NOT NULL
BEGIN
   DROP PROCEDURE EliminarFactura_sp;
END 
go
create PROCEDURE [dbo].EliminarFactura_sp
 @IdFactura int 
AS
BEGIN
	DELETE FROM [dbo].[Factura]
      WHERE IdFactura=@IdFactura
END
