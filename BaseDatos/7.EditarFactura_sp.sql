
-- =============================================
-- Author:		Sandra Moron
-- Create date: 27/06/2017
-- Description:	Editar una Factura
-- =============================================


IF OBJECT_ID('dbo.EditarFactura_sp') IS NOT NULL
BEGIN
   DROP PROCEDURE EditarFactura_sp;
END 

go
CREATE PROCEDURE EditarFactura_sp
	@IdFactura int, 
	@Descripcion nvarchar(max) 

AS
BEGIN
	UPDATE [dbo].[Factura]
    set  [Descripcion] = @Descripcion
 WHERE IdFactura =@IdFactura

END
GO
