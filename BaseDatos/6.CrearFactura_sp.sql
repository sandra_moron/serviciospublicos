
-- =============================================
-- Author:		Sandra Moron
-- Create date: 27/06/2017
-- Description:	Crear Factura
-- =============================================


IF OBJECT_ID('dbo.CrearFactura_sp') IS NOT NULL
BEGIN
   DROP PROCEDURE CrearFactura_sp;
END 

go
CREATE PROCEDURE CrearFactura_sp
	-- Add the parameters for the stored procedure here
	
	@NumeroContrato nvarchar(50) ,
	@Descripcion nvarchar(max) ,
	@IdUsuario int 
AS
BEGIN
	INSERT INTO [dbo].[Factura]
           ([NumeroContrato]
           ,[Descripcion]
           ,[IdUsuario])
     VALUES
           (@NumeroContrato
           ,@Descripcion
           ,@IdUsuario)
END
GO
