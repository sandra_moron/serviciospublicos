﻿using FacturaWebServicio.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FacturaWebSitio.Controllers
{
    public class FacturasController : Controller
    {
        // GET: Facturas
        public async Task<ActionResult> Index()
        {
			var httpClient = new HttpClient();
			var result = await httpClient.GetStringAsync("http://localhost:54039/api/Facturas");
			List<Factura> listaFacturas = JsonConvert.DeserializeObject<List<Factura>>(result);
			return View(listaFacturas);
		}

        // GET: Facturas/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Facturas/Create
        public ActionResult Create()
        {
            return View();
        }

		[HttpPost]
		public async Task<ActionResult> Create(Factura factura)
		{
			if (factura.NumeroContrato == null)
			{
				ModelState.AddModelError("NumeroContrato", "Obligatorio");
			}
			//var existe = ExisteFactura(factura.NumeroContrato);
			var httpClient = new HttpClient();
			var result1 = await httpClient.GetStringAsync("http://localhost:54039/api/Facturas/ExisteFactura/" + factura.NumeroContrato);
			int existe = JsonConvert.DeserializeObject<int>(result1);

			if (existe == 1)
			{
				ModelState.AddModelError("NumeroContrato", "Ya existe una factura asociada a este contrato");
			}

			if (ModelState.IsValid)
			{				
				 httpClient = new HttpClient();
				factura.IdUsuario = 1;
				httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
				var data = JsonConvert.SerializeObject(factura);
				var contentPost = new StringContent(data, Encoding.UTF8, "application/json");
				var result = await httpClient.PostAsync("http://localhost:54039/api/Facturas", contentPost);
				//var result = await httpClient.GetAsync(string.Format("http://localhost:54039/api/Facturas", factura));
				return RedirectToAction("Index");
			}
			else
			{
				return View(factura);
			}
		}


		// GET: Facturas/Edit/5
		public async Task<ActionResult> Edit(int id)
        {
			var httpClient = new HttpClient();
			var result = await httpClient.GetStringAsync("http://localhost:54039/api/Facturas/" + id);
			Factura factura = JsonConvert.DeserializeObject<Factura>(result);
			return View(factura);
		}

        // POST: Facturas/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, Factura factura)
        {
            try
            {
				var httpClient = new HttpClient();			
				if (ModelState.IsValid)
				{
					httpClient = new HttpClient();
					httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
					factura.IdFactura = id;
					var data = JsonConvert.SerializeObject(factura);
					var contentPost = new StringContent(data, Encoding.UTF8, "application/json");
					var result = await httpClient.PutAsync("http://localhost:54039/api/Facturas", contentPost);
					//var result = await httpClient.PutAsync("http://localhost:54039/api/Facturas/" + id);
					return RedirectToAction("Index");
				}
				else
				{
					return View(factura);
				}
			}
            catch
            {
                return View();
            }
        }

        // GET: Facturas/Delete/5
        public async Task<ActionResult> Delete(int id)
		{
			try
			{
				var httpClient = new HttpClient();
				var result = await httpClient.DeleteAsync("http://localhost:54039/api/Facturas/" + id);				
				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}			
		}


		//public async Task<bool> ExisteFactura(string numContrato)
		//{
		//	var httpClient = new HttpClient();
		//	var result = await httpClient.GetStringAsync("http://localhost:54039/api/Facturas/ExisteFactura/" + numContrato);
		//	int existe = JsonConvert.DeserializeObject<int>(result);
		//	if (existe == 0)
		//	{
		//		return false;
		//	}
		//	return true;

		//}

		// POST: Facturas/Delete/5
		[HttpPost]
		public ActionResult Delete(FormCollection collection)
		{
			try
			{
				if (collection["IdFactura"]!= null && collection["IdFactura"].Count() > 0)
				{
					string[] ids = collection["IdFactura"].Split(new char[] { ',' });
					foreach (string id in ids)
					{
						var httpClient = new HttpClient();
						var result = httpClient.DeleteAsync("http://localhost:54039/api/Facturas/" + id);
					}					
				}

				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}
	}
}
