﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FacturaWebSitio.Startup))]
namespace FacturaWebSitio
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
